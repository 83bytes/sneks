import random
import sys


board_len = 100

def dice():
    return random.randint(1,6)

def sneks_ladders():
    # this is number of places in the board that will be "infected" by either a snake or a ladder
    occupation = 40
    t1 = [int(board_len * random.random()) for x in range(int(occupation/2))]
    t2 = [int(board_len * random.random()) for x in range(int(occupation/2))]
    snek_board = dict(zip(t1, t2))
    return snek_board

def play_turn(pos, snek_board, player):
    dice_val = dice()
    new_pos = pos + dice_val
    print(player + " dice has value " + str(dice_val))
    
    if (new_pos in snek_board.keys()):
        nn_pos = snek_board[new_pos]
        
        #need this here because only this branch is for when it hits a snake or a ladder
        if (nn_pos > new_pos):
            # this means our bro hit a ladder
            print(player + " hit a ladder at " + str(new_pos) + " and went to " + str(nn_pos))
        else:
            # our bro hit a snake
            print(player + " hit a snake at " + str(new_pos) + " and went to " + str(nn_pos))
            
        return nn_pos
    elif (new_pos == 100):
        print(player + " has won the game")
        sys.exit(0)
    elif (new_pos > board_len):
        return pos
    else:
        return new_pos

def run_game():
    player_1_pos = 0
    player_2_pos = 0
    flag = True
    snek_board = sneks_ladders()
    
    while True:
        print("Current Positions are:")
        print("Player 1 :", player_1_pos)
        print("Player 2 :", player_2_pos)
        print("\n")
        print("Hit Enter for turn. Ctrl-C for exit. Both players will advance by 1 turn")
        input()
        player_1_pos = play_turn(player_1_pos, snek_board, "Player 1")
        player_2_pos = play_turn(player_2_pos, snek_board, "Player 2")
